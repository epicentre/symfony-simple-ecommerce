# Simple Symfony E-Commerce App

## Installation

* Clone or download project
* You can change the variables in the optional .env file(DOCKER_MYSQL_USER, DOCKER_MYSQL_DATABASE, ...)
* Start containers (docker-compose up --build -d)
* Run migration and fixtures creation commands. In this step, the data will be created in the permanent database. You don't need to run this command again.

```bash

docker exec -it sec_php composer install
docker exec -it sec_php php bin/console doctrine:database:create --if-not-exists
docker exec -it sec_php php bin/console doctrine:migrations:migrate -n
docker exec -it sec_php php bin/console doctrine:fixtures:load -n

```

## Usage

Default Web Url: http://localhost:8081

Default Admin User-Pass: cwepicentre@gmail.com:123456

Default User-Pass: user1@gmail.com:123456