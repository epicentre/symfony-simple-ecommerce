<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Product[]
     */
    public function findAllForAdminList(): array
    {
        return $this->createQueryBuilder('p')
            ->addSelect('pu, pi, pc')
            ->innerJoin('p.user', 'pu')
            ->leftJoin('p.images', 'pi')
            ->leftJoin('p.categories', 'pc')
            ->orderBy('p.updatedAt', 'DESC')
            ->where('p.deletedAt IS NULL')
            ->getQuery()
            ->getResult();
    }

    public function findAllActiveProducts()
    {
        return $this->createQueryBuilder('p')
            ->addSelect('pi, pc')
            ->leftJoin('p.images', 'pi')
            ->leftJoin('p.categories', 'pc')
            ->orderBy('p.updatedAt', 'DESC')
            ->where('p.deletedAt IS NULL')
            ->andWhere('p.isActive = :isActive')->setParameter('isActive', true)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $productId
     * @return Product|null
     */
    public function findActiveProductById(int $productId): ?Product
    {
        try {
            return $this->createQueryBuilder('p')
                ->where('p.id = :productId')->setParameter('productId', $productId)
                ->andWhere('p.deletedAt IS NULL')
                ->andWhere('p.isActive = :isActive')->setParameter('isActive', true)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
