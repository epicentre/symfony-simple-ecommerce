<?php

namespace App\Controller;

use App\Service\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        if ($this->getUser()) {
            $products = $this->productService->getAllActiveProducts();
        }

        return $this->render('home/index.html.twig', [
            'products' => $products ?? []
        ]);
    }
}
