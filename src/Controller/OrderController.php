<?php

namespace App\Controller;

use App\DTO\UserOrderRequest;
use App\Service\OrderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/order")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class OrderController extends AbstractController
{
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @Route("/order", methods={"POST"}, name="order")
     */
    public function order(UserOrderRequest $request): Response
    {
        $this->orderService->order($this->getUser(), $request);

        $this->addFlash('success', 'Siparişiniz alındı.');

        return $this->redirectToRoute('home');
    }
}
