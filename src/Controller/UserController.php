<?php

namespace App\Controller;

use App\DTO\UserRegisterRequest;
use App\Security\LoginFormAuthenticator;
use App\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * @Route("/user")
 * @IsGranted("IS_ANONYMOUS")
 */
class UserController extends AbstractController
{
    /**
     * @var UserService
     */
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @Route("/register", methods="GET", name="user_register_form")
     */
    public function registerForm(): Response
    {
        return $this->render('user/register.html.twig');
    }

    /**
     * @Route("/register", methods="POST", name="user_register")
     */
    public function register(Request $request, UserRegisterRequest $registerDTO, GuardAuthenticatorHandler $authenticatorHandler, LoginFormAuthenticator $authenticator): Response
    {
        $user = $this->userService->createUserByRegisterForm($registerDTO);

        $this->addFlash('success', 'Kayıt işleminiz başarıyla gerçekleşti.');

        return $authenticatorHandler->authenticateUserAndHandleSuccess($user, $request, $authenticator, 'main');
        // return $this->redirectToRoute('home');
    }
}
