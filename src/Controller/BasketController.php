<?php

namespace App\Controller;

use App\Exception\ValidationException;
use App\Service\BasketService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/basket")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class BasketController extends AbstractController
{
    /**
     * @var BasketService
     */
    private $basketService;

    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    /**
     * @Route("/", methods={"GET"}, name="basket")
     */
    public function index(): Response
    {
        $basketData = $this->basketService->getBasketData($this->getUser());

        return $this->render('basket/index.html.twig', [
            'basketData' => $basketData
        ]);
    }

    /**
     * @Route("/add", methods={"POST"}, name="add_basket")
     */
    public function add(Request $request)
    {
        $productId = (int) $request->get('productId', 0);
        $quantity = (int) $request->get('quantity', 0);
        if ($productId < 1 || $quantity < 1) {
            throw new ValidationException('Hata');
        }

        $this->basketService->addBasket($this->getUser(), $productId, $quantity);

        return $this->redirectToRoute('basket');
    }

    /**
     * @Route("/{id<\d+>}/delete-product", methods="POST", name="delete_product_basket")
     */
    public function deleteProduct(int $id)
    {
        $this->basketService->deleteProduct($this->getUser(), $id);

        $this->addFlash('success', 'Ürün sepetinizden silindi.');

        return $this->redirectToRoute('basket');
    }
}
