<?php

namespace App\Controller\Admin;

use App\DTO\CategoryAddEditRequest;
use App\Entity\Category;
use App\Service\CategoryService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/category")
 * @IsGranted("ROLE_ADMIN")
 */
class CategoryController extends AbstractController
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Route("/", methods="GET", name="admin_categories")
     */
    public function index(): Response
    {
        return $this->render('admin/category/index.html.twig', [
            'categories' => $this->categoryService->getAllCategories(),
        ]);
    }

    /**
     * @Route("/add", methods="GET", name="admin_category_add")
     */
    public function add()
    {
        return $this->render('admin/category/add.html.twig');
    }

    /**
     * @Route("/add", methods="POST", name="admin_category_insert")
     */
    public function insert(CategoryAddEditRequest $request)
    {
        $this->categoryService->addCategory($request, $this->getUser());

        $this->addFlash('success', 'Kategori başarıyla eklendi.');

        return $this->redirectToRoute('admin_categories');
    }

    /**
     * @Route("/{id<\d+>}/edit", methods="GET", name="admin_category_edit")
     */
    public function edit(Category $category)
    {
        return $this->render('admin/category/edit.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods="POST", name="admin_category_update")
     */
    public function update(CategoryAddEditRequest $request, Category $category)
    {
        $this->categoryService->updateProduct($request, $category);

        $this->addFlash('success', 'Kategori başarıyla güncelendi.');

        return $this->redirectToRoute('admin_categories');
    }
}
