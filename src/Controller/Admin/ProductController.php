<?php

namespace App\Controller\Admin;

use App\DTO\ProductAddEditRequest;
use App\Entity\Product;
use App\Service\CategoryService;
use App\Service\ProductService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/product")
 * @IsGranted("ROLE_ADMIN")
 */
class ProductController extends AbstractController
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryService = $categoryService;
    }

    /**
     * @Route("/", methods="GET", name="admin_products")
     */
    public function index(): Response
    {
        return $this->render('admin/product/index.html.twig', [
            'products' => $this->productService->getAllProductsForAdmin(),
        ]);
    }

    /**
     * @Route("/add", methods="GET", name="admin_product_add")
     */
    public function add()
    {
        return $this->render('admin/product/add.html.twig', [
            'categories' => $this->categoryService->getAllActiveCategories()
        ]);
    }

    /**
     * @Route("/add", methods="POST", name="admin_product_insert")
     */
    public function insert(ProductAddEditRequest $request)
    {
        $this->productService->addProduct($request, $this->getUser());

        $this->addFlash('success', 'Ürün başarıyla eklendi.');

        return $this->redirectToRoute('admin_products');
    }

    /**
     * @Route("/{id<\d+>}/edit", methods="GET", name="admin_product_edit")
     */
    public function edit(Product $product)
    {
        return $this->render('admin/product/edit.html.twig', [
            'product' => $product,
            'categories' => $this->categoryService->getAllActiveCategories()
        ]);
    }

    /**
     * @Route("/{id<\d+>}/edit", methods="POST", name="admin_product_update")
     */
    public function update(ProductAddEditRequest $request, Product $product)
    {
        $this->productService->updateProduct($request, $product);

        $this->addFlash('success', 'Ürün başarıyla güncelendi.');

        return $this->redirectToRoute('admin_products');
    }

    /**
     * @Route("/{id<\d+>}/delete", methods="POST", name="admin_product_delete")
     */
    public function delete(Product $product)
    {
        $this->productService->deleteProduct($product);

        $this->addFlash('success', 'Ürün başarıyla silindi.');

        return $this->redirectToRoute('admin_products');
    }
}
