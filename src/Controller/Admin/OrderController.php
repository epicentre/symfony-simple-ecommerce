<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Service\OrderService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/order")
 * @IsGranted("ROLE_ADMIN")
 */
class OrderController extends AbstractController
{
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @Route("/admin/order", methods={"GET"}, name="admin_orders")
     */
    public function index(): Response
    {
        return $this->render('admin/order/index.html.twig', [
            'orders' => $this->orderService->getAllOrders(),
        ]);
    }

    /**
     * @Route("/{id<\d+>}/approve", methods="POST", name="admin_order_approve")
     */
    public function approve(Order $order)
    {
        $this->orderService->updateOrderStatus($order, Order::STATUS_APPROVED);

        $this->addFlash('success', 'Sipariş onaylandı.');

        return $this->redirectToRoute('admin_orders');
    }

    /**
     * @Route("/{id<\d+>}/reject", methods="POST", name="admin_order_reject")
     */
    public function reject(Order $order)
    {
        $this->orderService->updateOrderStatus($order, Order::STATUS_REJECTED);

        $this->addFlash('success', 'Sipariş reddedildi.');

        return $this->redirectToRoute('admin_orders');
    }
}
