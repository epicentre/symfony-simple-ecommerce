<?php

namespace App\Service;

use App\DTO\CategoryAddEditRequest;
use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Entity\ProductImage;
use App\Entity\User;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

class CategoryService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->categoryRepository = $this->entityManager->getRepository(Category::class);
    }

    /**
     * @return Category[]
     */
    public function getAllCategories(): array
    {
        return $this->categoryRepository->findBy([], ['updatedAt' => 'DESC']);
    }

    /**
     * @return Category[]
     */
    public function getAllActiveCategories(): array
    {
        return $this->categoryRepository->findBy(['isActive' => true], ['updatedAt' => 'DESC']);
    }

    /**
     * @param int $categoryId
     * @return Category|null
     */
    public function getCategoryById(int $categoryId): ?Category
    {
        return $this->categoryRepository->findOneBy(['id' => $categoryId, 'isActive' => true]);
    }

    /**
     * @param CategoryAddEditRequest $request
     * @param Category $category
     */
    public function updateProduct(CategoryAddEditRequest $request, Category $category): void
    {
        $category
            ->setName($request->getName())
            ->setIsActive($request->isActive());

        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }

    /**
     * @param CategoryAddEditRequest $request
     * @param User $user
     */
    public function addCategory(CategoryAddEditRequest $request, User $user): void
    {
        $category = (new Category())
            ->setUser($user)
            ->setName($request->getName())
            ->setIsActive($request->isActive());

        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
}
