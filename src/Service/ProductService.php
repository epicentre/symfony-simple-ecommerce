<?php

namespace App\Service;

use App\DTO\ProductAddEditRequest;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Entity\ProductImage;
use App\Entity\User;
use App\Repository\ProductCategoryRepository;
use App\Repository\ProductImageRepository;
use App\Repository\ProductRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class ProductService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var CategoryService
     */
    private $categoryService;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var ProductImageRepository
     */
    private $productImageRepository;

    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;

    public function __construct(EntityManagerInterface $entityManager, SluggerInterface $slugger, CategoryService $categoryService)
    {
        $this->entityManager = $entityManager;
        $this->productRepository = $this->entityManager->getRepository(Product::class);
        $this->productImageRepository = $this->entityManager->getRepository(ProductImage::class);
        $this->productCategoryRepository = $this->entityManager->getRepository(ProductCategory::class);
        $this->slugger = $slugger;
        $this->categoryService = $categoryService;
    }

    /**
     * @return Product[]
     */
    public function getAllProductsForAdmin(): array
    {
        return $this->productRepository->findAllForAdminList();
    }

    /**
     * @return Product[]
     */
    public function getAllActiveProducts(): array
    {
        return $this->productRepository->findAllActiveProducts();
    }

    /**
     * @param int $productId
     * @return Product|null
     */
    public function getActiveProductById(int $productId): ?Product
    {
        return $this->productRepository->findActiveProductById($productId);
    }

    /**
     * @param ProductAddEditRequest $request
     * @param Product $product
     */
    public function updateProduct(ProductAddEditRequest $request, Product $product): void
    {
        $product
            ->setName($request->getName())
            ->setSlug($this->slugger->slug($request->getName())->lower())
            ->setDescription($request->getDescription())
            ->setPrice($request->getPrice())
            ->setIsActive($request->isActive());

        $this->entityManager->persist($product);

        $this->productImageRepository->removeByProductId($product->getId());
        if ($imageUrls = array_filter($request->getImageUrls())) {
            foreach ($imageUrls as $url) {
                $productImage = (new ProductImage())
                    ->setProduct($product)
                    ->setUrl($url);

                $this->entityManager->persist($productImage);
            }
        }

        $this->productCategoryRepository->removeByProductId($product->getId());
        if ($categories = array_filter($request->getCategories())) {
            foreach ($categories as $categoryId) {
                if ($category = $this->categoryService->getCategoryById($categoryId)) {
                    $productCategory = (new ProductCategory())
                        ->setProduct($product)
                        ->setCategory($category);

                    $this->entityManager->persist($productCategory);
                }
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param ProductAddEditRequest $request
     * @param User $user
     */
    public function addProduct(ProductAddEditRequest $request, User $user): void
    {
        $product = (new Product())
            ->setUser($user)
            ->setName($request->getName())
            ->setSlug($this->slugger->slug($request->getName())->lower())
            ->setDescription($request->getDescription())
            ->setPrice($request->getPrice())
            ->setIsActive($request->isActive());

        $this->entityManager->persist($product);

        if ($imageUrls = array_filter($request->getImageUrls())) {
            foreach ($imageUrls as $url) {
                $productImage = (new ProductImage())
                    ->setProduct($product)
                    ->setUrl($url);

                $this->entityManager->persist($productImage);
            }
        }

        if ($categories = array_filter($request->getCategories())) {
            foreach ($categories as $categoryId) {
                if ($category = $this->categoryService->getCategoryById($categoryId)) {
                    $productCategory = (new ProductCategory())
                        ->setProduct($product)
                        ->setCategory($category);

                    $this->entityManager->persist($productCategory);
                }
            }
        }

        $this->entityManager->flush();
    }

    /**
     * @param Product $product
     */
    public function deleteProduct(Product $product): void
    {
        $product->setDeletedAt(new DateTime());

        $this->entityManager->persist($product);
        $this->entityManager->flush();
    }
}
