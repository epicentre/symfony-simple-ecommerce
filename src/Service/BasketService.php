<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class BasketService
{
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var ProductService
     */
    private $productService;

    public function __construct(CacheInterface $cache, ProductService $productService)
    {
        $this->cache = $cache;
        $this->productService = $productService;
    }

    public function addBasket(User $user, int $productId, int $quantity)
    {
        $basketData = [];
        $cacheBasketData = $this->cache->getItem('basket_data_' . $user->getId());
        if (!$cacheBasketData->isHit()) {
            $basketData[$productId] = $quantity;
        } else {
            $basketData = $cacheBasketData->get();
            $basketData[$productId] = isset($basketData[$productId]) ? $basketData[$productId] + $quantity : $quantity;
        }

        $cacheBasketData->expiresAfter(60 * 60 * 48); // 48 hour
        $cacheBasketData->set($basketData);
        $this->cache->save($cacheBasketData);
    }

    public function deleteProduct(User $user, int $productId)
    {
        $cacheBasketData = $this->cache->getItem('basket_data_' . $user->getId());
        if ($cacheBasketData->isHit()) {
            $basketData = $cacheBasketData->get();
            unset($basketData[$productId]);

            $cacheBasketData->expiresAfter(60 * 60 * 48); // 48 hour
            $cacheBasketData->set($basketData);
            $this->cache->save($cacheBasketData);
        }
    }

    /**
     * @param User $user
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function flushBasket(User $user): void
    {
        $this->cache->delete('basket_data_' . $user->getId());
    }

    /**
     * @param User $user
     * @return array
     */
    public function getBasketData(User $user): array
    {
        $data = [
            'products' => [],
            'total' => 0
        ];

        $cacheBasketData = $this->cache->getItem('basket_data_' . $user->getId())->get();
        if (!empty($cacheBasketData)) {
            foreach ($cacheBasketData as $productId => $quantity) {
                if ($product = $this->productService->getActiveProductById($productId)) {
                    $data['products'][] = compact('product', 'quantity');
                    $data['total'] += $product->getPrice() * $quantity;
                }
            }
        }

        return $data;
    }
}
