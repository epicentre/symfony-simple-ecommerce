<?php

namespace App\Service;

use App\DTO\UserOrderRequest;
use App\Entity\Order;
use App\Entity\OrderDetail;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Repository\OrderRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var BasketService
     */
    private $basketService;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(EntityManagerInterface $entityManager, BasketService $basketService)
    {
        $this->entityManager = $entityManager;
        $this->orderRepository = $this->entityManager->getRepository(Order::class);
        $this->basketService = $basketService;
    }

    /**
     * @param User $user
     * @param UserOrderRequest $request
     * @throws ValidationException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function order(User $user, UserOrderRequest $request): void
    {
        $basketData = $this->basketService->getBasketData($user);
        if (count($basketData['products']) < 1) {
            throw new ValidationException('Sepetinde ürün bulunamadı. Lütfen tekrar dene.');
        }

        $order = (new Order())
            ->setCustomer($user)
            ->setOrderNumber(rand(100000, 999999))
            ->setPrice($basketData['total'])
            ->setAddress($request->getAddress())
            ->setDate(new DateTime())
            ->setStatus(Order::STATUS_WAITING)
            ->setPaymentType($request->getPaymentType())
            ->setComment($request->getComment());

        $this->entityManager->persist($order);

        foreach ($basketData['products'] as $productData) {
            $orderDetail = (new OrderDetail())
                ->setOrderr($order)
                ->setProduct($productData['product'])
                ->setPrice($productData['product']->getPrice())
                ->setQuantity($productData['quantity'])
                ->setTotal($productData['product']->getPrice() * $productData['quantity']);

            $this->entityManager->persist($orderDetail);
        }

        $this->entityManager->flush();
        $this->basketService->flushBasket($user);
    }

    public function getAllOrders()
    {
        return $this->orderRepository->findBy([], ['updatedAt' => 'DESC']);
    }

    public function updateOrderStatus(Order $order, int $status)
    {
        $order->setStatus($status);

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
}
