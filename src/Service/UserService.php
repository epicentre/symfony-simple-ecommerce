<?php

namespace App\Service;

use App\DTO\UserRegisterRequest;
use App\Entity\User;
use App\Exception\ValidationException;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $this->entityManager->getRepository(User::class);
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @return User[]
     */
    public function getAllUsers(): array
    {
        return $this->userRepository->findAll();
    }

    /**
     * @param UserRegisterRequest $request
     * @return User
     * @throws ValidationException
     */
    public function createUserByRegisterForm(UserRegisterRequest $request): User
    {
        if ($user = $this->userRepository->findOneBy(['email' => $request->getEmail()])) {
            throw new ValidationException('Girdiğiniz email adresi sistemde kayıtlı');
        }

        $user = (new User())
            ->setFullName($request->getFullName())
            ->setEmail($request->getEmail())
            ->setPhone($request->getPhone())
            ->setRoles(['ROLE_USER'])
            ->setCreatedAt(new DateTime());

        $user->setPassword($this->userPasswordEncoder->encodePassword($user, $request->getPassword()));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
