<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\ProductCategory;
use App\Entity\ProductImage;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private const USER_DATA = [
        ['Arif DEMİR', 'cwepicentre@gmail.com', '123456', '5457325141', ['ROLE_ADMIN']],
        ['User 1', 'user1@gmail.com', '123456', null, ['ROLE_USER']],
        ['User 2', 'user2@gmail.com', '123456', null, ['ROLE_USER']],
        ['User 3', 'user3@gmail.com', '123456', null, ['ROLE_USER']],
    ];

    private const CATEGORY_DATA = [
        'Elektronik',
        'Moda',
        'Anne, Bebek',
        'Kitap',
        'Spor',
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder, SluggerInterface $slugger)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadCategories($manager);
        $this->loadProducts($manager);
    }

    private function loadUsers(ObjectManager $manager): void
    {
        foreach (self::USER_DATA as [$fullName, $email, $password, $phone, $roles]) {
            $user = (new User())
                ->setFullName($fullName)
                ->setEmail($email)
                ->setPhone($phone)
                ->setRoles($roles)
                ->setCreatedAt(new DateTime());

            $user->setPassword($this->userPasswordEncoder->encodePassword($user, $password));
            $this->addReference($email, $user);
            $manager->persist($user);
        }

        $manager->flush();
    }

    private function loadCategories(ObjectManager $manager): void
    {
        foreach (self::CATEGORY_DATA as $name) {
            $category = (new Category())
                ->setName($name)
                ->setIsActive(true);

            $category->setUser($this->getReference(self::USER_DATA[0][1]));
            $this->addReference('category_' . $name, $category);
            $manager->persist($category);
        }

        $manager->flush();
    }

    private function loadProducts(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 20; $i++) {
            $name = 'Ürün ' . $i;

            $product = (new Product())
                ->setName($name)
                ->setSlug($this->slugger->slug($name)->lower())
                ->setPrice(rand(10, 100))
                ->setIsActive(true);

            $product->setUser($this->getReference(self::USER_DATA[0][1]));
            $manager->persist($product);

            $productImage = (new ProductImage())
                ->setProduct($product)
                ->setUrl('https://picsum.photos/200/300');

            $manager->persist($productImage);

            for ($j = 1; $j <= rand(1, 3); $j++) {
                $productCategory = (new ProductCategory())
                    ->setProduct($product)
                    ->setCategory($this->getReference('category_' . array_rand(array_flip(self::CATEGORY_DATA))));

                $manager->persist($productCategory);
            }
        }

        $manager->flush();
    }
}
