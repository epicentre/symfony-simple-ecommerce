<?php

namespace App\Util;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait Timestampable
{
    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var DateTimeInterface
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     * Don't call manually
     */
    public function setCreatedAtAuto(): void
    {
        if (!$this->getCreatedAt()) {
            $now = new DateTime();
            $this->createdAt = $now;
            $this->updatedAt = $now;
        }
    }

    /**
     * @ORM\PreUpdate
     * Don't call manually
     */
    public function setUpdatedAtAuto(): void
    {
        $this->updatedAt = new DateTime();
    }
}
