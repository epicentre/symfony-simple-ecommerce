<?php

namespace App\DTO;

use App\Http\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class ProductAddEditRequest implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank(message="Ürün ismi alanı boş bırakılamaz.")
     */
    private $name;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var float
     * @Assert\NotBlank(message="Fiyat alanı boş bırakılamaz.")
     * @Assert\Type(type="numeric", message="Fiyat alanı numeric olmalıdır.")
     */
    private $price;

    /**
     * @var array
     * @Assert\Count(min=1, minMessage="En az 1 tane ürün resmi girilmelidir.")
     */
    private $imageUrls = [];

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var bool
     */
    private $isActive;

    public function __construct(Request $request)
    {
        $this->name = $request->get('name');
        $this->description = $request->get('description');
        $this->price = $request->get('price');
        $this->imageUrls = (array) $request->get('images');
        $this->categories = (array) $request->get('categories', []);
        $this->isActive = (bool) $request->get('isActive', false);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return array
     */
    public function getImageUrls(): array
    {
        return $this->imageUrls;
    }

    /**
     * @param array $imageUrls
     * @return $this
     */
    public function setImageUrls(array $imageUrls): self
    {
        $this->imageUrls = $imageUrls;
        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return $this
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }
}
