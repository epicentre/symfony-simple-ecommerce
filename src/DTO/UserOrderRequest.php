<?php

namespace App\DTO;

use App\Http\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UserOrderRequest implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank(message="Adres alanı boş bırakılamaz.")
     */
    private $address;

    /**
     * @var int
     */
    private $paymentType;

    /**
     * @var string|null
     */
    private $comment;

    public function __construct(Request $request)
    {
        $this->address = $request->get('address');
        $this->paymentType = (int) $request->get('paymentType', 1);
        $this->comment = $request->get('comment');
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentType(): int
    {
        return $this->paymentType;
    }

    /**
     * @param int $paymentType
     * @return $this
     */
    public function setPaymentType(int $paymentType): self
    {
        $this->paymentType = $paymentType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;
        return $this;
    }
}
