<?php

namespace App\DTO;

use App\Http\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegisterRequest implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank(message="Ad soyad alanı boş bırakılamaz.")
     */
    private $fullName;

    /**
     * @var string
     * @Assert\Email(message="E-mail doğru formatta olmaldırı.")
     */
    private $email;

    /**
     * @var string
     * @Assert\Length(
     *      min = 6,
     *      minMessage = "Şifreniz en az 6 karakterden oluşmalıdır"
     * )
     * @Assert\NotBlank()
     * @Assert\EqualTo(
     *     propertyPath="passwordConfirmation",
     *     message="Şifreleriniz aynı olmaldır."
     * )
     */
    private $password;

    /**
     * @var string
     */
    private $passwordConfirmation;

    /**
     * @var string|null
     */
    private $phone;

    public function __construct(Request $request)
    {
        $this->fullName = $request->get('fullName');
        $this->email = $request->get('email');
        $this->password = $request->get('password');
        $this->passwordConfirmation = $request->get('passwordConfirmation');
        $this->phone = $request->get('phone');
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->fullName;
    }

    /**
     * @param string $fullName
     * @return $this
     */
    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordConfirmation(): string
    {
        return $this->passwordConfirmation;
    }

    /**
     * @param string $passwordConfirmation
     * @return $this
     */
    public function setPasswordConfirmation(string $passwordConfirmation): self
    {
        $this->passwordConfirmation = $passwordConfirmation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }
}
