<?php

namespace App\DTO;

use App\Http\RequestDTOInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CategoryAddEditRequest implements RequestDTOInterface
{
    /**
     * @var string
     * @Assert\NotBlank(message="İsim alanı boş bırakılamaz.")
     */
    private $name;

    /**
     * @var bool
     */
    private $isActive;

    public function __construct(Request $request)
    {
        $this->name = $request->get('name');
        $this->isActive = (bool) $request->get('isActive', false);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;
        return $this;
    }
}
